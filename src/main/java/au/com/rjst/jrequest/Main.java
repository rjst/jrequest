package au.com.rjst.jrequest;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class Main {
    public static void main(String[] args) {
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();

            HttpGet httpget = new HttpGet(args[args.length - 1]);

            System.out.println("Executing request " + httpget.getRequestLine());
            HttpResponse respo = httpclient.execute(httpget);
            respo.getEntity().writeTo(System.out);

        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
}
